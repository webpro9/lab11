import "reflect-metadata"
import { DataSource } from "typeorm"
import { User } from "./entity/User"
import { Product } from "./entity/Product"

export const AppDataSource = new DataSource({
    type: "sqlite",
    database: "DB.sqlite",
    synchronize: true,
    logging: false,
    entities: [],
    migrations: [],
    subscribers: [],
})
